Source: python-catalogue
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-setuptools,
               python3-pytest <!nocheck>,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-catalogue
Vcs-Git: https://salsa.debian.org/python-team/packages/python-catalogue.git
Homepage: https://github.com/explosion/catalogue
Rules-Requires-Root: no

Package: python3-catalogue
Architecture: any
Depends: ${python3:Depends},
         ${misc:Depends},
Description: super lightweight function registries for Python3 libraries
 catalogue is a tiny, zero-dependencies library that makes it easy to add
 function (or object) registries to your code. Function registries are
 helpful when you have objects that need to be both easily serializable
 and fully customizable. Instead of passing a function into your object,
 you pass in an identifier name, which the object can use to lookup the
 function from the registry. This makes the object easy to serialize,
 because the name is a simple string. If you instead saved the function,
 you'd have to use Pickle for serialization, which has many drawbacks.
